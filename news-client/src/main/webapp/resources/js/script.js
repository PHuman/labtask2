function changePage(pageId) {
	var url = window.location.toString();
	window.location = url.replace(/page={1}\d*/, 'page=' + pageId);
}

function viewNews(id) {
	var url = window.location.toString();
	var newurl = window.location.search.toString().replace(/page={1}\d*&/, '');
	if (url.indexOf("search") > -1) {
		window.location = 'NewsServlet'
				+ newurl.replace(/search/, 'viewbysearch') + '&id=' + id;
	} else {
		window.location = "NewsServlet?id=" + id + "&action=view";
	}
}

function viewByCriteria(id) {
	var url = window.location.toString();
	var newurl = window.location.search.toString().replace(/id={1}\d*/,
			'id=' + id);
	if (url.indexOf("search") > -1) {
		window.location = "NewsServlet" + newurl + "&action=viewbysearch";
	} else {
		window.location = "NewsServlet?id=" + id + "&action=view";
	}
}
