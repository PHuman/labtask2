<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="/news-client/resources/js/script.js"></script>
<link rel="stylesheet" type="text/css"
	href="/news-client/resources/css/style.css">

<title>News</title>
</head>
<body>
	<div class="header">
		<h1>News portal</h1>
	</div>
	<div>
		<table>
			<tr>
				<td><a href="#" onclick="window.location ='NewsServlet?page=1&action=list';">MAIN PAGE</a></td>
			</tr>
			<tr>
				<td><b>${newsTO.news.title}</b></td>
				<td>(by ${newsTO.author.name})</td>
				<td><fmt:formatDate pattern="dd/MM/yyyy"
						value="${newsTO.news.modificationDate}" /></td>
			</tr>
			<tr>
				<td>${newsTO.news.fullText}<br>
				</td>
			</tr>
			<c:forEach var="comment" items="${newsTO.comments}">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy"
							value="${comment.creationDate}" /></td>
				</tr>
				<tr>
					<td>${comment.text}</td>
				</tr>
			</c:forEach>
		</table>
		<form action="NewsServlet" method="post">
			<input type="hidden" value="savecomment" name="action">
			<textarea rows="3" cols="40" maxlength="100" name="comment"></textarea>
			<br> <input type="hidden" value="${newsTO.news.id}" name="id">
			<input type="submit" value="Post comment" />
		</form>
		<table>
			<tr>
				<c:if test="${newsTO.news.prev > 0}">
					<td align="left"><a href="#"
						onclick="viewByCriteria(${newsTO.news.prev})">PREVIOUS</a></td>
				</c:if>
				<c:if test="${newsTO.news.next > 0}">
					<td align="right"><a href="#"
						onclick="viewByCriteria(${newsTO.news.next})">NEXT</a></td>
				</c:if>
			</tr>
		</table>
	</div>
	<div class="footer">Copyright @Epam 2016. All rights reserved.</div>
</body>
</html>