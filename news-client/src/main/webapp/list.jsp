<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="/news-client/resources/js/script.js"></script>
<link rel="stylesheet" type="text/css"
	href="/news-client/resources/css/style.css">
<title>News</title>
</head>
<body>
	<div class="header">
		<h1>News portal</h1>
	</div>
	<div>
		<form action="NewsServlet" method="get">
			<input type="hidden" name="action" value="search"/> <input
				type="hidden" name="page" value="1"/>
			<table>
				<tr>
					<td><select name="author">
							<option selected="selected" disabled="disabled">Select
								Authors</option>
							<c:forEach var="author" items="${authors}">
								<option value="${author.id}">${author.name}</option>
							</c:forEach>
					</select></td>
					<td>
						<div id="list" class="dropdown-check-list" tabindex="100"
							align="left">
							<span class="anchor">Select Tags</span>
							<ul id="items" class="items">
								<c:forEach var="tag" items="${tags}">
									<li><input type="checkbox" value="${tag.id}" name="tags" />${tag.name}</li>
								</c:forEach>
							</ul>
						</div>
					</td>
					<td class="button"><input type="submit" value="Filter" /> <input
						type="button" value="Reset"
						onclick="window.location ='NewsServlet?page=1&action=list';"/></td>
				</tr>
			</table>
		</form>
		<table>
			<c:forEach var="newslist" items="${news}">
				<tr>
					<td><b><a href="#" onclick="viewNews(${newslist.news.id})">${newslist.news.title}</a>
					</b></td>
					<td></td>
					<td>(by ${newslist.author.name})</td>
					<td align="right"><fmt:formatDate pattern="dd/MM/yyyy"
							value="${newslist.news.modificationDate}" /></td>
				</tr>
				<tr>
					<td>${newslist.news.shortText}</td>
				</tr>
				<tr>
					<td></td>
					<td><c:forEach var="tag" items="${newslist.tags}">
						${tag.name}
						</c:forEach></td>
					<td>Comments(${newslist.comments.size()})</td>
				</tr>
				<tr>
					<td><br></td>
				</tr>
			</c:forEach>
		</table>
		<c:if test="${count>1}">
			<c:forEach begin="1" end="${count}" varStatus="loop">
				<a class="link" href="#" onclick="changePage(${loop.index})">${loop.index}</a>
			</c:forEach>
		</c:if>
		<script>
		var checkList = document.getElementById('list');
		var items = document.getElementById('items');
		checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
			if (items.className.indexOf('visible') > -1) {
				items.className -= 'visible';
				items.style.display = "none";
			} else {
				items.className += 'visible';
				items.style.display = "block";
			}
		}
		items.onblur = function(evt) {
			items.classList.remove('visible');
		}
	</script>
	</div>
	<br>
	<div class="footer">Copyright @Epam 2016. All rights reserved.</div>
</body>
</html>