package com.epam.news.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.command.NewsCommand;
import com.epam.news.util.Mapping;

public class Dispatcher {
	@Autowired
	private NewsCommand command;

	public String execute(String value, HttpServletRequest request) {
		String result = "/error.jsp";
		for (Method method : command.getClass().getMethods()) {
			if (method.getAnnotation(Mapping.class) != null
					&& value.equals(method.getAnnotation(Mapping.class).value())) {
				try {
					result = (String) method.invoke(command, request);

				} catch (IllegalAccessException | InvocationTargetException e) {
					e.printStackTrace();
					result = "/error.jsp";
				}
			}
		}
		return result;
	}
}
