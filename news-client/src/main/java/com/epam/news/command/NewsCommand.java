package com.epam.news.command;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.CommentService;
import com.epam.news.service.FacadeNewsService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import com.epam.news.util.Mapping;

public class NewsCommand {
	private static int COUNT = 5;
	@Autowired
	private FacadeNewsService facadeNewsService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private CommentService commentService;

	@Mapping(value = "list")
	public String index(HttpServletRequest request) throws ServiceException {
		String p = request.getParameter("page");
		int page;
		if (p != null) {
			page = Integer.parseInt(request.getParameter("page"));
		} else {
			page = 1;
		}
		long all = newsService.countNews();
		int count = (int) Math.ceil((double) all / COUNT);
		List<NewsTO> news = facadeNewsService.loadAllNews(page);
		request.setAttribute("tags", tagService.loadAll());
		request.setAttribute("authors", authorService.loadAll());
		request.setAttribute("news", news);
		request.setAttribute("count", count);
		return "/list.jsp";
	}

	@Mapping(value = "view")
	public String viewNews(HttpServletRequest request) throws ServiceException {
		long id = Long.parseLong(request.getParameter("id"));
		NewsTO news = facadeNewsService.loadNews(id);
		request.setAttribute("newsTO", news);
		return "/news.jsp";
	}

	@Mapping(value = "viewbysearch")
	public String viewNewsByCriteria(HttpServletRequest request) throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		String aut = request.getParameter("author");
		long author;
		if (aut != null) {
			author = Long.parseLong(request.getParameter("author"));
		} else {
			author = 0;
		}
		String[] tags = request.getParameterValues("tags");
		Long id = Long.parseLong(request.getParameter("id"));
		criteria.setAuthor(new Author(author));
		if (tags != null) {
			criteria.setTags(Arrays.stream(tags).map(Tag::new).collect(Collectors.toList()));
		}
		NewsTO news = facadeNewsService.loadNewsByCriteria(criteria, id);
		request.setAttribute("newsTO", news);
		return "/news.jsp";
	}

	@Mapping(value = "search")
	public String search(HttpServletRequest request) throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		String aut = request.getParameter("author");
		long author = 0;
		if (aut != null) {
			author = Long.parseLong(aut);
		}
		String[] tags = request.getParameterValues("tags");
		int page = Integer.parseInt(request.getParameter("page"));
		if (aut == null && tags == null) {
			return "/list.jsp";
		}
		if (aut != null) {
			criteria.setAuthor(new Author(author));
		}
		if (tags != null) {
			criteria.setTags(Arrays.stream(tags).map(Tag::new).collect(Collectors.toList()));
		}
		long all = newsService.countNewsBySearch(criteria);
		int count = (int) Math.ceil((double) all / COUNT);
		List<NewsTO> news = facadeNewsService.findAllNews(criteria, page);
		request.setAttribute("tags", tagService.loadAll());
		request.setAttribute("authors", authorService.loadAll());
		request.setAttribute("news", news);
		request.setAttribute("count", count);
		return "/list.jsp";
	}

	@Mapping(value = "savecomment")
	public String saveComment(HttpServletRequest request) throws ServiceException {
		long id = Long.parseLong(request.getParameter("id"));
		String name = request.getParameter("comment");
		Comment comment = new Comment(id, name, Timestamp.valueOf(LocalDateTime.now()));
		commentService.save(comment);
		return "/NewsServlet?action=view&id=" + id;
	}

}
