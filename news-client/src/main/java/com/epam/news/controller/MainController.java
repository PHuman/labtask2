package com.epam.news.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.news.util.Dispatcher;

@WebServlet(name = "NewsServlet", urlPatterns = "/")
public class MainController extends HttpServlet {
	ClassPathXmlApplicationContext context;
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		context = new ClassPathXmlApplicationContext("root-context.xml");
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Dispatcher dispatcher = (Dispatcher) context.getBean("dispatcher");
		String action = request.getParameter("action");
		String page = dispatcher.execute(action, request);
		RequestDispatcher dispatch = getServletContext().getRequestDispatcher(page);
		dispatch.forward(request, response);
	}
}