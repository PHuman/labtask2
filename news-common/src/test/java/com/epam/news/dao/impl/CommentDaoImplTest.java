package com.epam.news.dao.impl;

import com.epam.news.dao.CommentDao;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("/comment-data.xml")
@DatabaseTearDown(value = "/comment-data.xml", type = DatabaseOperation.DELETE_ALL)
public class CommentDaoImplTest {
    @Autowired
    private CommentDao commentDao;

    @Test
    public void testSave() throws DaoException{
        Comment comment = new Comment(2,"TestComment", Timestamp.valueOf(LocalDateTime.now()));
        commentDao.save(comment);
        List<Comment> load = commentDao.load(2);
        assertEquals(load.get(0).getText(), comment.getText());
    }
}
