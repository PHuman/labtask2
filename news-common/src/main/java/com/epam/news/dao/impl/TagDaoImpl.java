package com.epam.news.dao.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.dao.TagDao;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TagDaoImpl implements TagDao {
	@Autowired
	private DataSource source;
	private static final Logger LOG = Logger.getLogger(TagDaoImpl.class);
	private static final String SQL_SAVE = "INSERT INTO TAGS (TAG_TAG_NAME) VALUES (?)";
	private static final String SQL_UPDATE = "UPDATE TAGS SET TAG_TAG_NAME=? WHERE TAG_TAG_ID = ?";
	private static final String SQL_DELETE_BY_ID = "DELETE FROM TAGS WHERE TAG_TAG_ID=?";
	private static final String SQL_LOAD_BY_NEWS_ID = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS INNER JOIN"
			+ " NEWS_TAGS ON TAG_TAG_ID = NTAG_TAG_ID WHERE NTAG_NEWS_ID = ?";
	private static final String SQL_LOAD_ALL = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS";
	private static final String SQL_LOAD_BY_NAME = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS WHERE TAG_TAG_NAME = ?";
	private static final String SQL_DELETE_LINK_TO_NEWS = "DELETE FROM NEWS_TAGS WHERE NTAG_TAG_ID = ?";

	/**
	 * @see TagDao#save(Tag)
	 */
	@Override
	public Long save(Tag tag) throws DaoException {
		Optional<Long> tagId;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new int[] { 1 })) {
			preparedStatement.setString(1, tag.getName());
			preparedStatement.execute();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			rs.next();
			tagId = Optional.of(rs.getLong(1));
		} catch (SQLException e) {
			throw new DaoException("Exception when Tag name = " + tag.getName(), e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return tagId.orElseThrow(() -> new DaoException("Exception when Tag name = " + tag.getName()));
	}

	/**
	 * @see TagDao#loadByName(String)
	 */
	@Override
	public Tag loadByName(String tagName) throws DaoException {
		Tag tag = null;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_BY_NAME)) {
			preparedStatement.setString(1, tagName);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				tag = new Tag(result.getLong(1), result.getString(2));
			}
		} catch (SQLException e) {
			throw new DaoException("Exception when Tag name = " + tagName, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return tag;
	}

	/**
	 * @see TagDao#load(Long)
	 */
	@Override
	public List<Tag> load(Long newsId) throws DaoException {
		List<Tag> tags = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				Tag tag = new Tag(result.getLong(1), result.getString(2));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("Exception when News id = " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return tags;
	}

	/**
	 * @see TagDao#loadAll()
	 */
	@Override
	public List<Tag> loadAll() throws DaoException {
		List<Tag> tags = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_ALL)) {
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				Tag tag = new Tag(result.getLong(1), result.getString(2));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return tags;
	}

	/**
	 * @see TagDao#update(Tag)
	 */
	@Override
	public void update(Tag tag) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			preparedStatement.execute();
		} catch (SQLException e) {
			throw new DaoException("Exception when Tag id = " + tag.getId(), e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see TagDao#delete(long)
	 */
	@Override
	public void delete(long tagId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
			preparedStatement.setLong(1, tagId);
			preparedStatement.execute();
		} catch (SQLException e) {
			throw new DaoException("Exception when Comment id " + tagId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see TagDao#deleteLinkToNews(long)
	 */
	@Override
	public void deleteLinkToNews(long tagId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_NEWS)) {
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Exception when Tag id = " + tagId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}
}
