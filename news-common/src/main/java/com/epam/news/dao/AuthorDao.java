package com.epam.news.dao;

import java.util.List;

import com.epam.news.domain.Author;
import com.epam.news.exception.DaoException;

public interface AuthorDao {
    /**
     * Save Author and return id
     *
     * @param author
     * @return id
     * @throws DaoException
     */
    long save(Author author) throws DaoException;

    /**
     * edit Author, Author must have authorId
     *
     * @param author
     * @throws DaoException
     */
    void update(Author author) throws DaoException;

    /**
     * Load Author for current News by News id
     *
     * @param newsId
     * @return Author
     * @throws DaoException
     */
    Author loadByNews(long newsId) throws DaoException;

    /**
     * Load Author by name
     *
     * @param authorName
     * @return Author
     * @throws DaoException
     */
    Author loadByName(String authorName) throws DaoException;
    
    /**
     * Load all Authors
     *
     * @return List<Author>
     * @throws DaoException
     */
    List<Author> loadAll() throws DaoException;
    
    /**
     * expire Author
     *
     * @param authorId
     * @throws DaoException
     */
	void expire(long authorId) throws DaoException;
    
}
