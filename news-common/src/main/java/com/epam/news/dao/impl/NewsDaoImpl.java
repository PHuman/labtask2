package com.epam.news.dao.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.domain.News;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NewsDaoImpl implements NewsDao {
	@Autowired
	private DataSource source;
	private static int COUNT = 5;
	private static final Logger LOG = Logger.getLogger(NewsDaoImpl.class);

	private static final String SQL_SAVE = "INSERT INTO NEWS (NWS_TITLE, NWS_SHORT_TEXT,"
			+ " NWS_FULL_TEXT) VALUES (?,?,?)";
	private static final String SQL_UPDATE = "UPDATE NEWS SET NWS_TITLE = ?, NWS_SHORT_TEXT = ?,"
			+ " NWS_FULL_TEXT = ?, NWS_MODIFICATION_DATE= ? WHERE NWS_NEWS_ID = ?";
	private static final String SQL_LOAD = "SELECT * FROM (SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, "
			+ "NWS_CREATION_DATE, NWS_MODIFICATION_DATE, "
			+ "LAG(NWS_NEWS_ID) OVER (ORDER BY NWS_MODIFICATION_DATE DESC) prev, "
			+ "LEAD(NWS_NEWS_ID) OVER (ORDER BY NWS_MODIFICATION_DATE DESC) next FROM NEWS) WHERE NWS_NEWS_ID =?";
	private static final String SQL_LOAD_ALL_SORTED = "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, "
			+ "NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM (SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, "
			+ "NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, ROW_NUMBER() OVER (ORDER BY "
			+ "NWS_MODIFICATION_DATE DESC) AS NUM FROM NEWS) WHERE NUM BETWEEN ? AND ?";
	private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NWS_NEWS_ID = ?";
	private static final String SQL_LINK_TO_AUTHOR = "INSERT INTO NEWS_AUTHORS (NAUT_NEWS_ID, NAUT_AUTHOR_ID) VALUES (?,?)";
	private static final String SQL_DELETE_LINK_TO_AUTHOR = "DELETE FROM NEWS_AUTHORS WHERE NAUT_NEWS_ID = ?";
	private static final String SQL_LINK_TO_TAGS = "INSERT INTO NEWS_TAGS (NTAG_NEWS_ID, NTAG_TAG_ID) VALUES (?,?)";
	private static final String SQL_DELETE_LINK_TO_TAGS = "DELETE FROM NEWS_TAGS WHERE NTAG_NEWS_ID = ?";
	private static final String SQL_COUNT = "SELECT COUNT(NWS_NEWS_ID) FROM NEWS";
	// query for SearchCriteria
	private static final String FIND_BY_CRITERIA = "SELECT DISTINCT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, "
			+ "NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM("
			+ "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE "
			+ ",DENSE_RANK() OVER (ORDER BY NWS_MODIFICATION_DATE DESC) AS NUM FROM NEWS ";
	private static final String END_FIND_BY_CRITERIA = ") WHERE NUM BETWEEN ? AND ? ORDER BY NWS_MODIFICATION_DATE DESC";
	private static final String COUNT_BY_CRITERIA = "SELECT COUNT (NWS_NEWS_ID) FROM("
			+ "SELECT DISTINCT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, "
			+ "NWS_MODIFICATION_DATE FROM NEWS ";
	private static final String END_COUNT_BY_CRITERIA = ")";
	private static final String LOAD_BY_CRITERIA = "SELECT * FROM (SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, "
			+ "NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, "
			+ "LAG(NWS_NEWS_ID) OVER (ORDER BY NWS_MODIFICATION_DATE DESC) prev, "
			+ "LEAD(NWS_NEWS_ID) OVER (ORDER BY NWS_MODIFICATION_DATE DESC) next FROM( "
			+ "SELECT DISTINCT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, "
			+ "NWS_MODIFICATION_DATE FROM NEWS ";
	private static final String END_LOAD_BY_CRITERIA = ")) WHERE NWS_NEWS_ID = ?";
	private static final String ADD_AUTHOR = "NAUT_AUTHOR_ID = ";
	private static final String ADD_TAGS = "NTAG_TAG_ID in(";
	private static final String INNER_TAGS = "INNER JOIN NEWS_TAGS ON  NWS_NEWS_ID = NTAG_NEWS_ID ";
	private static final String INNER_AUTHORS = "INNER JOIN NEWS_AUTHORS ON NWS_NEWS_ID = NAUT_NEWS_ID ";

	/**
	 * @see NewsDao#save(News)
	 */
	@Override
	public Long save(News news) throws DaoException {
		Optional<Long> newsId;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new int[] { 1 })) {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.execute();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			rs.next();
			newsId = Optional.of(rs.getLong(1));
		} catch (SQLException e) {
			throw new DaoException("Exception when News title = " + news.getTitle(), e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return newsId.orElseThrow(() -> new DaoException("Exception when News title = " + news.getTitle()));
	}

	/**
	 * @see NewsDao#edit(News)
	 */
	@Override
	public void edit(News news) throws DaoException {
		news.setModificationDate(Date.valueOf(LocalDate.now()));
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setDate(4, news.getModificationDate());
			preparedStatement.setLong(5, news.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Exception when News title = " + news.getTitle(), e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#delete(List)
	 */
	@Override
	public void delete(List<Long> newsId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
			for (Long id : newsId) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("Exception when delete", e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#loadAllNews()
	 */
	@Override
	public List<News> loadAllNews(int page) throws DaoException {
		int begin = 1 + COUNT * (page - 1);
		int end = page * COUNT;
		List<News> news = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_ALL_SORTED)) {
			preparedStatement.setLong(1, begin);
			preparedStatement.setLong(2, end);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				News currentNews = new News(result.getLong(1), result.getString(2), result.getString(3),
						result.getString(4), result.getTimestamp(5), result.getDate(6));
				news.add(currentNews);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return news;
	}

	/**
	 * @see NewsDao#load(Long)
	 */
	@Override
	public News load(long newsId) throws DaoException {
		News news = null;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD)) {
			preparedStatement.setLong(1, newsId);
			ResultSet result = preparedStatement.executeQuery();
			result.next();
			news = new News(result.getLong(1), result.getString(2), result.getString(3), result.getString(4),
					result.getTimestamp(5), result.getDate(6));
			news.setPrev(result.getLong(7));
			news.setNext(result.getLong(8));
		} catch (SQLException e) {
			throw new DaoException("Exception when id = " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return news;
	}

	/**
	 * @see NewsDao#saveLinkToAuthor(Long, Long)
	 */
	@Override
	public void saveLinkToAuthor(Long authorId, Long newsId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LINK_TO_AUTHOR)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.execute();
		} catch (SQLException e) {
			throw new DaoException("Exception when Author id = " + authorId + " News id = " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#deleteLinkToAuthor(List)
	 */
	@Override
	public void deleteLinkToAuthor(List<Long> newsId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_AUTHOR)) {
			for (Long id : newsId) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("Exception when News id = " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#saveLinkToTags(List, Long)
	 */
	@Override
	public void saveLinkToTags(List<Long> tagsId, Long newsId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LINK_TO_TAGS)) {
			for (Long tagId : tagsId) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("Exception when newsId" + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#deleteLinkToTags(List)
	 */
	@Override
	public void deleteLinkToTags(List<Long> newsId) throws DaoException {
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_TAGS)) {
			for (Long id : newsId) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("Exception when News id = " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
	}

	/**
	 * @see NewsDao#countNews()
	 */
	@Override
	public Long countNews() throws DaoException {
		Optional<Long> count;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_COUNT)) {
			ResultSet result = preparedStatement.executeQuery();
			result.next();
			count = Optional.of(result.getLong(1));
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return count.orElseThrow(DaoException::new);
	}

	/**
	 * @see NewsDao#loadByCriteria(SearchCriteria, Long)
	 */
	@Override
	public News loadByCriteria(SearchCriteria criteria, long newsId) throws DaoException {
		News news = null;
		String query = criteriaBuilder(criteria, LOAD_BY_CRITERIA);
		query += END_LOAD_BY_CRITERIA;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setLong(1, newsId);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				news = new News(result.getLong(1), result.getString(2), result.getString(3), result.getString(4),
						result.getTimestamp(5), result.getDate(6));
				news.setPrev(result.getLong(7));
				news.setNext(result.getLong(8));
			}
		} catch (SQLException e) {
			throw new DaoException("Exception when id =    " + newsId, e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return news;
	}

	/**
	 * @see NewsDao#countNewsBySearch()
	 */
	@Override
	public Long countNewsBySearch(SearchCriteria criteria) throws DaoException {
		Optional<Long> count;
		String query = criteriaBuilder(criteria, COUNT_BY_CRITERIA);
		query += END_COUNT_BY_CRITERIA;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			ResultSet result = preparedStatement.executeQuery();
			result.next();
			count = Optional.of(result.getLong(1));
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return count.orElseThrow(DaoException::new);
	}

	/**
	 * @see NewsDao#findNews(SearchCriteria, int)
	 */
	@Override
	public List<News> findNews(SearchCriteria criteria, int page) throws DaoException {
		List<News> news = new ArrayList<>();
		String query = criteriaBuilder(criteria, FIND_BY_CRITERIA);
		query += END_FIND_BY_CRITERIA;
		Connection connection = DataSourceUtils.getConnection(source);
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setInt(1, 1 + COUNT * (page - 1));
			preparedStatement.setInt(2, COUNT * page);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				News currentNews = new News(result.getLong(1), result.getString(2), result.getString(3),
						result.getString(4), result.getTimestamp(5), result.getDate(6));
				news.add(currentNews);
			}
		} catch (SQLException e) {
			throw new DaoException("Exception when Author id = " + criteria.getAuthor().getId(), e);
		} finally {
			DataSourceUtils.releaseConnection(connection, source);
		}
		return news;
	}

	private String criteriaBuilder(SearchCriteria criteria, String query) {
		boolean authorIsExist = false;
		boolean tagIsExist = false;
		StringBuilder result = new StringBuilder(query);
		if (criteria.getAuthor() != null) {
			result.append(INNER_AUTHORS);
			authorIsExist = true;
		}
		if (criteria.getTags() != null && !criteria.getTags().isEmpty()) {
			result.append(INNER_TAGS);
			tagIsExist = true;
		}
		if (authorIsExist || tagIsExist) {
			result.append("WHERE ");
			if (authorIsExist) {
				result.append(ADD_AUTHOR).append(criteria.getAuthor().getId());
			}
			if (tagIsExist && authorIsExist) {
				result.append(" AND ");
			}
			if (tagIsExist) {
				result.append(ADD_TAGS);
				for (Tag tag : criteria.getTags()) {
					result.append(tag.getId()).append(",");
				}
				result.deleteCharAt(result.length() - 1);
				result.append(")");
			}
		}
		return result.toString();
	}
}
