package com.epam.news.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.validation.constraints.Pattern;

public class News implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9_&();:\"^\\- ]{1,100}")
	private String title;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9_&();:\"^\\- ]{1,100}")
	private String shortText;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9_&();:\"^\\- ]{1,2000}")
	private String fullText;
	private Timestamp creationDate;
	private Date modificationDate;
	private long prev;
	private long next;

	public News() {
	}

	public News(long id, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		this.setId(id);
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getPrev() {
		return prev;
	}

	public void setPrev(long prev) {
		this.prev = prev;
	}

	public long getNext() {
		return next;
	}

	public void setNext(long next) {
		this.next = next;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
