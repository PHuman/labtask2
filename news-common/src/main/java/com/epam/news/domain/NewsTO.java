package com.epam.news.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

public class NewsTO implements Serializable {
	@Valid
	private News news;
	@Valid
	private Author author;
	private List<Tag> tags;
	private List<Comment> comments;

	public NewsTO() {
	}

	public NewsTO(News news) {
		super();
		this.news = news;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "NewsTO [news=" + news + ", author=" + author + ", tags=" + tags + ", comments=" + comments + "]";
	}

}
