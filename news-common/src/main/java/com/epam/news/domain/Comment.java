package com.epam.news.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.Pattern;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9_&();:\"^\\- ]{1,100}")
	private String text;
	private Timestamp creationDate;

	public Comment() {
	}

	public Comment(long id, String text, Timestamp creationDate) {
		this.setId(id);
		this.text = text;
		this.creationDate = creationDate;
	}

	public Comment(long id, String text) {
		this.setId(id);
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
