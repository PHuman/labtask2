package com.epam.news.domain;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9_&();:\"^\\- ]{1,30}")
	private String name;

	public Tag() {
	}

	public Tag(String name) {
		this.name = name;
	}

	public Tag(long id, String name) {
		this.setId(id);
		this.name = name;
	}

	public Tag(long id) {
		this.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
