package com.epam.news.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.Pattern;

public class Author implements Serializable{
	private static final long serialVersionUID = 1L;
	private long id;
	@Pattern(regexp = "[а-яА-ЯA-Za-z0-9]{1,30}")
	private String name;
	private Timestamp expired;

	public Author() {
	}

	public Author(String name) {
		this.name = name;
	}

	public Author(long id) {
		this.setId(id);
	}

	public Author(long id, String name) {
		this.setId(id);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getExpired() {
		return expired;
	}

	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
