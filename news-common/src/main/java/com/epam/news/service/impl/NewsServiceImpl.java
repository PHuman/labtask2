package com.epam.news.service.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.domain.Tag;
import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NewsServiceImpl implements NewsService {
	@Autowired
	private NewsDao newsDao;

	/**
	 * @see NewsService#saveNews(NewsTO)
	 */
	@Transactional
	@Override
	public void saveNews(NewsTO newsTO) throws ServiceException {
		long newsId;
		try {
			newsId = newsDao.save(newsTO.getNews());
			newsDao.saveLinkToAuthor(newsTO.getAuthor().getId(), newsId);
			if (newsTO.getTags() != null) {
				newsDao.saveLinkToTags(newsTO.getTags().stream().map(Tag::getId).collect(Collectors.toList()),
						newsId);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#editNews(NewsTO)
	 */
	@Transactional
	@Override
	public void editNews(NewsTO newsTO) throws ServiceException {
		try {
			newsDao.edit(newsTO.getNews());
			if (newsTO.getTags() != null) {
				List<Long> list = new ArrayList<>();
				list.add(newsTO.getNews().getId());
				newsDao.deleteLinkToTags(list);
				newsDao.saveLinkToTags(newsTO.getTags().stream().map(Tag::getId).collect(Collectors.toList()),
						newsTO.getNews().getId());
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#deleteNews(List)
	 */
	@Transactional
	@Override
	public void deleteNews(List<Long> newsId) throws ServiceException {
		try {
			newsDao.deleteLinkToAuthor(newsId);
			newsDao.deleteLinkToTags(newsId);
			newsDao.delete(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#countNews()
	 */
	@Override
	public long countNews() throws ServiceException {
		try {
			return newsDao.countNews();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#loadNews(long)
	 */
	@Override
	public News loadNews(long newsId) throws ServiceException {
		try {
			return newsDao.load(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * @see NewsService#loadNews(long)
	 */
	@Override
	public News loadByCriteria(SearchCriteria criteria, long newsId) throws ServiceException {
		try {
			return newsDao.loadByCriteria(criteria, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#loadAllNews()
	 */
	@Override
	public List<News> loadAllNews(int page) throws ServiceException {
		try {
			return newsDao.loadAllNews(page);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#findNews(SearchCriteria, int)
	 */
	@Override
	public List<News> findNews(SearchCriteria criteria, int page) throws ServiceException {
		try {
			return newsDao.findNews(criteria, page);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#countNewsBySearch(SearchCriteria)
	 */
	@Override
	public Long countNewsBySearch(SearchCriteria criteria) throws ServiceException {
		try {
			return newsDao.countNewsBySearch(criteria);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
