package com.epam.news.service.impl;

import com.epam.news.dao.TagDao;
import com.epam.news.dao.impl.TagDaoImpl;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class TagServiceImpl implements TagService {
	@Autowired
	private TagDao tagDao;
	private static final Logger LOG = Logger.getLogger(TagServiceImpl.class);

	/**
	 * @see TagService#save(Tag)
	 */
	@Override
	public boolean save(Tag tag) throws ServiceException {
		boolean flag = false;
		try {
			if (tagDao.loadByName(tag.getName()) == null) {
				tagDao.save(tag);
				flag = true;
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see TagService#load(Long)
	 */
	@Override
	public List<Tag> load(Long newsId) throws ServiceException {
		try {
			return tagDao.load(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * @see TagService#loadAll
	 */
	@Override
	public List<Tag> loadAll() throws ServiceException {
		try {
			return tagDao.loadAll();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * @see TagService#update(Tag)
	 */
	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDao.update(tag);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * @see TagService#delete(long)
	 */
	@Transactional
	@Override
	public void delete(long tagId) throws ServiceException {
		try {
			tagDao.deleteLinkToNews(tagId);
			tagDao.delete(tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}		
	}
}
