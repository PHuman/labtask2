package com.epam.news.service;

import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface FacadeNewsService {
	/**
	 * delete News by id
	 *
	 * @param newsId
	 * @throws ServiceException
	 */
	void deleteNews(List<Long> newsId) throws ServiceException;

	/**
	 * @return all news with author and tags sorted by modification date and by
	 * 
	 * @throws ServiceException
	 */
	List<NewsTO> loadAllNews(int page) throws ServiceException;

	/**
	 * return News with author and comments by newsId
	 * 
	 * @param newsId
	 * @return NewsTO
	 * @throws ServiceException
	 */
	NewsTO loadNews(long newsId) throws ServiceException;

	/**
	 * @return all news with author and tags sorted by modification date and by
	 *         SearchCriteria
	 * 
	 * @throws ServiceException
	 */
	List<NewsTO> findAllNews(SearchCriteria criteria, int page) throws ServiceException;

	/**
	 * return News with author and comments by newsId and Criteria
	 * 
	 * @param newsId, criteria
	 * @return NewsTO
	 * @throws ServiceException
	 */
	NewsTO loadNewsByCriteria(SearchCriteria criteria, long newsId) throws ServiceException;
}
