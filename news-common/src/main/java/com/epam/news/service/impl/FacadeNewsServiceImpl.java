package com.epam.news.service.impl;

import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class FacadeNewsServiceImpl implements FacadeNewsService {
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;

	/**
	 * @see FacadeNewsService#deleteNews(List)
	 */
	@Transactional
	@Override
	public void deleteNews(List<Long> newsId) throws ServiceException {
		commentService.deleteByNewsId(newsId);
		newsService.deleteNews(newsId);
	}

	/**
	 * @see FacadeNewsService#loadAllNews(int)
	 */
	@Transactional
	@Override
	public List<NewsTO> loadAllNews(int page) throws ServiceException {
		List<NewsTO> transferObjects = new ArrayList<>();
		for (News news : newsService.loadAllNews(page)) {
			NewsTO transferObject = new NewsTO();
			transferObject.setNews(news);
			transferObject.setAuthor(authorService.loadByNews(news.getId()));
			transferObject.setTags(tagService.load(news.getId()));
			transferObject.setComments(commentService.loadByNewsId(news.getId()));
			transferObjects.add(transferObject);
		}
		return transferObjects;
	}

	/**
	 * @see FacadeNewsService#loadNews(long)
	 */
	@Transactional
	@Override
	public NewsTO loadNews(long newsId) throws ServiceException {
		NewsTO transferObject = new NewsTO();
		transferObject.setNews(newsService.loadNews(newsId));
		transferObject.setAuthor(authorService.loadByNews(newsId));
		transferObject.setTags(tagService.load(newsId));
		transferObject.setComments(commentService.loadByNewsId(newsId));
		return transferObject;
	}
	
	/**
	 * @see FacadeNewsService#loadNewsByCriteria(SearchCriteria,long)
	 */
	@Transactional
	@Override
	public NewsTO loadNewsByCriteria(SearchCriteria criteria, long newsId) throws ServiceException {
		NewsTO transferObject = new NewsTO();
		transferObject.setNews(newsService.loadByCriteria(criteria, newsId));
		transferObject.setAuthor(authorService.loadByNews(newsId));
		transferObject.setTags(tagService.load(newsId));
		transferObject.setComments(commentService.loadByNewsId(newsId));
		return transferObject;
	}
	
	/**
	 * @see FacadeNewsService#findAllNews(SearchCriteria, int)
	 */
	@Transactional
	@Override
	public List<NewsTO> findAllNews(SearchCriteria criteria, int page) throws ServiceException {
		List<NewsTO> transferObjects = new ArrayList<>();
		for (News news : newsService.findNews(criteria, page)) {
			NewsTO transferObject = new NewsTO();
			transferObject.setNews(news);
			transferObject.setAuthor(authorService.loadByNews(news.getId()));
			transferObject.setTags(tagService.load(news.getId()));
			transferObject.setComments(commentService.loadByNewsId(news.getId()));
			transferObjects.add(transferObject);
		}
		return transferObjects;
	}
}
