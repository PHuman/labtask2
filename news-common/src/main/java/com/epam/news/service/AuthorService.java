package com.epam.news.service;

import java.util.List;

import com.epam.news.domain.Author;
import com.epam.news.exception.ServiceException;

public interface AuthorService {
	/**
	 * save Author and return true if operation is success
	 *
	 * @param author
	 * @return true if operation is success
	 * @throws ServiceException
	 */
	boolean save(Author author) throws ServiceException;

	/**
	 * load Author by News id
	 *
	 * @param newsId
	 * @return author
	 * @throws ServiceException
	 */
	Author loadByNews(Long newsId) throws ServiceException;

	/**
	 * load all Authors
	 *
	 * @return List<Author>
	 * @throws ServiceException
	 */
	List<Author> loadAll() throws ServiceException;

	/**
	 * expire Author
	 *
	 * @param Author
	 * @throws ServiceException
	 */
	void expire(long authorId) throws ServiceException;

	/**
	 * update Author name
	 *
	 * @param authorId
	 * @throws ServiceException
	 */
	void update(Author author) throws ServiceException;

	
}
