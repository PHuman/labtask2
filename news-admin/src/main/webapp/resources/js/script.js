function view(id) {
	document.getElementById("name" + id).removeAttribute("readonly");
	document.getElementById("view" + id).hidden = true;
	document.getElementById("cancel" + id).hidden = false;
}
function hide(id) {
	document.getElementById("name" + id).setAttribute("readonly", "true");
	document.getElementById("view" + id).hidden = false;
	document.getElementById("cancel" + id).hidden = true;
}
function updateTag(id) {
	document.forms["updateTag" + id].submit();
}
function deleteTag(id) {
	document.forms["deleteTag" + id].submit();
}
function saveTag() {
	document.forms["saveTag"].submit();
}

function updateAuthor(id) {
	document.forms["updateAuthor" + id].submit();
}
function expireAuthor(id) {
	document.forms["expireAuthor" + id].submit();
}
function saveAuthor() {
	document.forms["saveAuthor"].submit();
}
function change(id) {
	if (id != 0)
		document.getElementById("form").setAttribute("action", "update");
}
function changePage(pageId) {
	var url = window.location.toString();
	window.location = url.replace(/page={1}\d*/, 'page=' + pageId);
}

function viewNews(id) {
	var url = window.location.toString();
	var newurl = window.location.search.toString().replace(/page={1}\d*&/, '');
	if (url.indexOf("search") > -1) {
		window.location = 'viewbysearch' + newurl + '&id=' + id;
	} else {
		window.location = "view?id=" + id;
	}
}

function viewByCriteria(id){
	var url = window.location.toString();
	var newurl = window.location.search.toString().replace(/id={1}\d*/, 'id='+id);
	if (url.indexOf("search") > -1) {
		window.location = newurl;
	} else {
		window.location = "view?id=" + id;
	}
}

