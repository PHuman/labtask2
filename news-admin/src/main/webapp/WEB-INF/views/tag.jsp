<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<script src="/news-admin/resources/js/script.js"></script>
<div>
	<table>
		<c:forEach var="tag" items="${tags}">
			<tr>
				<td>Tag:</td>
				<td><f:form method="post" action="updatetag"
						id="updateTag${tag.id}">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<f:input id="name${tag.id}" type="text" value="${tag.name}"
							readonly="readonly" path="name" pattern="[а-яА-ЯёЁa-zA-Z]{1,30}"
							required="required" />
						<f:errors path="name"></f:errors>
						<f:input path="id" type="hidden" value="${tag.id}" />
					</f:form></td>
				<td><form method="post" action="deletetag"
						id="deleteTag${tag.id}">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" /> <input name="id" type="hidden"
							value="${tag.id}">
					</form></td>
				<td><a id="view${tag.id}" href="#" onclick="view(${tag.id})">edit</a>
					<div id="cancel${tag.id}" hidden="">
						<a href="#" onclick="updateTag(${tag.id})">update</a> <a href="#"
							onclick="deleteTag(${tag.id})">delete</a> <a href="#"
							onclick="hide(${tag.id})">cancel</a>
					</div></td>
			</tr>
		</c:forEach>
	</table>
	<f:form method="post" action="savetag" id="saveTag">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" /> Add Tag: <f:input path="name" type="text"
			pattern="[а-яА-ЯёЁa-zA-Z]{1,30}" required="required" />
		<f:errors path="name"></f:errors>
		<a href="#" onclick="saveTag()">save</a>
	</f:form>
</div>