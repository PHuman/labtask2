<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/news-admin/resources/js/script.js"></script>
<div>
	<form action="search" method="get">
		<input type="hidden" name="page" value="1">
		<table>
			<tr>
				<td><select name="author">
						<option selected="selected" disabled="disabled">Select Authors</option>
							<c:forEach var="author" items="${authors}">
								<option value="${author.id}">${author.name}</option>
							</c:forEach>
				</select></td>
				<td>
					<div id="list" class="dropdown-check-list" tabindex="100"
						align="left">
						<span class="anchor">Select Tags</span>
						<ul id="items" class="items">
							<c:forEach var="tag" items="${tags}">
								<li><input type="checkbox" value="${tag.id}" name="tags" />${tag.name}</li>
							</c:forEach>
						</ul>
					</div>
				</td>
				<td class="button"><input type="submit" value="Filter" /> <input
					type="button" value="Reset"
					onclick="window.location = 'index?page=1';" /></td>
			</tr>
		</table>
	</form>
	<form action="delete" method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<table>
			<c:forEach var="newslist" items="${news}">
				<tr>
					<td><b><a href="#" onclick="viewNews(${newslist.news.id})">${newslist.news.title}</a>
					</b></td>
					<td></td>
					<td>(by ${newslist.author.name})</td>
					<td align="right"><fmt:formatDate pattern="dd/MM/yyyy"
							value="${newslist.news.modificationDate}" /></td>
				</tr>
				<tr>
					<td>${newslist.news.shortText}</td>
				</tr>
				<tr>
					<td></td>
					<td><c:forEach var="tag" items="${newslist.tags}">
						${tag.name}
						</c:forEach></td>
					<td>Comments(${newslist.comments.size()})</td>
					<td align="right"><a href="edit?id=${newslist.news.id}">Edit</a>
						<input name="id" type="checkbox" value="${newslist.news.id}"></td>
				</tr>
				<tr>
					<td><br></td>
				</tr>
			</c:forEach>
			<tr>
				<td><input type="submit" value="Delete" /></td>
			</tr>
		</table>
	</form>
	<c:if test="${count>1}">
		<c:forEach begin="1" end="${count}" varStatus="loop">
			<a class="link" href="#" onclick="changePage(${loop.index})">${loop.index}</a>
		</c:forEach>
	</c:if>
	<script>
		var checkList = document.getElementById('list');
		var items = document.getElementById('items');
		checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
			if (items.className.indexOf('visible') > -1) {
				items.className -= 'visible';
				items.style.display = "none";
			} else {
				items.className += 'visible';
				items.style.display = "block";
			}
		}
		items.onblur = function(evt) {
			items.classList.remove('visible');
		}
	</script>
</div>