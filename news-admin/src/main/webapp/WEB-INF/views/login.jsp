<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div>
	<form action="login" method="POST">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" /> <input type="text"
			pattern="[а-яА-ЯёЁa-zA-Z]{4,10}" required="required" name="username" />
		<input type="password" pattern=".{5,20}" required="required"
			name="password" /> <input type="submit" value="login" />
	</form>
</div>