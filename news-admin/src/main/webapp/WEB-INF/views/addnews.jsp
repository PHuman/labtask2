<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<script src="/news-admin/resources/js/script.js"></script>

<div>
	<f:form method="post" action="savenews" modelAttribute="newsto"
		id="form">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<table>
			<tr>
				<td>Title:</td>
				<td><f:input type="text" required="required" path="news.title"
						size="40" value="${newsto.news.title}" /> <f:errors
						path="news.title"></f:errors></td>
			</tr>
			<tr>
				<td>Brief:</td>
				<td><f:textarea rows="3" cols="42" maxlength="2000"
						path="news.shortText" required="required"
						value="${newsto.news.shortText}"></f:textarea> <f:errors
						path="news.shortText"></f:errors></td>
			</tr>
			<tr>
				<td>Content:</td>
				<td><f:textarea rows="7" cols="42" maxlength="2000"
						path="news.fullText" required="required"
						value="${newsto.news.fullText}"></f:textarea> <f:errors
						path="news.fullText"></f:errors></td>
			</tr>
			<tr id="tags" hidden="hidden">
				<td>Tags:</td>
				<td><c:forEach var="tag" items="${newsto.tags}">
						${tag.name}
						</c:forEach></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="padding-right: 54px"><select
					name="author" id="author">
						<c:forEach var="author" items="${authors}">
							<option value="${author.id}">${author.name}</option>
						</c:forEach>
				</select>
					<div id="list" class="dropdown-check-list" tabindex="100"
						align="left" style="margin-top: 0px">
						<span class="anchor">Select Tags</span>
						<ul id="items" class="items">
							<c:forEach var="tag" items="${tags}">
								<li><input type="checkbox" value="${tag.id}" name="tags" />${tag.name}</li>
							</c:forEach>
						</ul>
					</div>
			</tr>
			<tr>
				<td></td>
				<td align="right" style="margin-top: 10px"><input type="submit"
					value="Save" onclick="change(${newsto.news.id})" /></td>
			</tr>
		</table>
	</f:form>
	<script>
		var checkList = document.getElementById('list');
		var items = document.getElementById('items');
		checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
			if (items.className.indexOf('visible') > -1) {
				items.className -= 'visible';
				items.style.display = "none";
			} else {
				items.className += 'visible';
				items.style.display = "block";
			}
		}
		items.onblur = function(evt) {
			items.classList.remove('visible');
		}
		var id = ${newsto.news.id};
		if (id != 0){
			document.getElementById("author").hidden = true;
			document.getElementById("tags").hidden = false;
		}			
	</script>
</div>