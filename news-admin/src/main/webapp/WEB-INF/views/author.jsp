<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<script src="/news-admin/resources/js/script.js"></script>
<div>
	<table>
		<c:forEach var="author" items="${authors}">
			<tr>
				<td>Author:</td>
				<td><f:form method="post" action="updateauthor"
						modelAttribute="author" id="updateAuthor${author.id}">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<f:input path="name" id="name${author.id}" type="text"
							value="${author.name}" readonly="true" name="name"
							pattern="[а-яА-ЯёЁa-zA-Z]{1,30}" required="required" />
						<f:errors path="name"></f:errors>
						<f:input path="id" name="id" type="hidden" value="${author.id}" />
					</f:form></td>
				<td><form method="post" action="expireauthor"
						id="expireAuthor${author.id}">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" /> <input name="id" type="hidden"
							value="${author.id}">
					</form></td>
				<td><a id="view${author.id}" href="#"
					onclick="view(${author.id})">edit</a>
					<div id="cancel${author.id}" hidden="">
						<a href="#" onclick="updateAuthor(${author.id})">update</a> <a
							href="#" onclick="expireAuthor(${author.id})">expire</a> <a
							href="#" onclick="hide(${author.id})">cancel</a>
					</div></td>
			<tr>
		</c:forEach>
	</table>
	<f:form method="post" action="saveauthor" modelAttribute="author"
		id="saveAuthor">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		Add Author: <f:input path="name" name="name" type="text"
			pattern="[а-яА-ЯёЁa-zA-Z]{1,30}" required="required" />
		<f:errors path="name"></f:errors>
		<a href="#" onclick="saveAuthor()">save</a>
	</f:form>
</div>