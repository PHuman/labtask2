<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<script src="/news-admin/resources/js/script.js"></script>
<div>
	<table>
		<tr>
			<td><b>${newsTO.news.title}</b></td>
			<td>(by ${newsTO.author.name})</td>
			<td><fmt:formatDate pattern="dd/MM/yyyy"
					value="${newsTO.news.modificationDate}" /></td>
		</tr>
		<tr>
			<td>${newsTO.news.fullText}<br>
			</td>
		</tr>
		<c:forEach var="comment" items="${newsTO.comments}">
			<tr>
				<td><fmt:formatDate pattern="dd/MM/yyyy"
						value="${comment.creationDate}" /></td>
			</tr>
			<tr>
				<td>${comment.text}</td>
				<td><form action="deletecomment" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
						<input type="hidden" value="${comment.id}" name="id"> <input
							type="hidden" value="${newsTO.news.id}" name="idnews"> <input
							type="submit" value="X" />
					</form></td>
			</tr>
		</c:forEach>
	</table>
	<f:form action="savecomment" modelAttribute="comment" method="post">
	<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<f:textarea path="text" rows="3" cols="40" maxlength="100"></f:textarea>
		<f:errors path="text"></f:errors>
		<br> <f:input path="id" type="hidden" value="${newsTO.news.id}"/>
		<input type="submit" value="Post comment" />
	</f:form>
	<table>
		<tr>
			<c:if test="${newsTO.news.prev > 0}">
				<td align="left"><a href="#" onclick="viewByCriteria(${newsTO.news.prev})">PREVIOUS</a></td>
			</c:if>
			<c:if test="${newsTO.news.next > 0}">
				<td align="right"><a href="#" onclick="viewByCriteria(${newsTO.news.next})">NEXT</a></td>
			</c:if>
		</tr>
	</table>
</div>