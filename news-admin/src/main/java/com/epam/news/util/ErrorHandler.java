package com.epam.news.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.news.exception.ServiceException;

/**
 * Class receives all exceptions and errors to log them and show an error page.
 */
@ControllerAdvice
public class ErrorHandler {

	private static final Logger LOG = Logger.getLogger(ErrorHandler.class);

	@ExceptionHandler(value = ServiceException.class)
	public String defaultErrorHandler(HttpServletRequest request, Exception exception) throws Exception {
		LOG.error("", exception);
		return "error";
	}
}