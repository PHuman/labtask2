package com.epam.news.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.news.domain.Author;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

@Secured(value = "ROLE_ADMIN")
@Controller
public class TagController {
	@Autowired
	private TagService tagService;

	@RequestMapping(value = "tag")
	public String viewTags(Model model) throws ServiceException {
		List<Tag> tags = tagService.loadAll();
		model.addAttribute("tags", tags);
		if (!model.containsAttribute("tag")) {
			model.addAttribute("tag", new Tag());
		}
		return "tag";
	}

	@RequestMapping(value = "deletetag")
	public String deleteTag(Model model, @RequestParam("id") long id) throws ServiceException {
		tagService.delete(id);
		return "redirect:tag";
	}

	@RequestMapping(value = "updatetag")
	public String updateTag(Model model, @ModelAttribute("tag") @Valid Tag tag, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.tag", result);
			redirect.addFlashAttribute("tag", tag);
		} else {
			tagService.update(tag);
		}
		return "redirect:tag";
	}

	@RequestMapping(value = "savetag")
	public String saveTag(Model model, @ModelAttribute("tag") @Valid Tag tag, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.tag", result);
			redirect.addFlashAttribute("tag", tag);
		} else {
			tagService.save(tag);
		}
		return "redirect:tag";
	}
}
