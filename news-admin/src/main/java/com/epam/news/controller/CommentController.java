package com.epam.news.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.news.domain.Comment;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;

@Secured(value = "ROLE_ADMIN")
@Controller
public class CommentController {
	private static final Logger LOG = Logger.getLogger(CommentController.class);
	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "savecomment")
	public String saveComment(Model model, @ModelAttribute("comment") @Valid Comment comment, BindingResult result,
			RedirectAttributes redirect, HttpServletRequest request) throws ServiceException {
		String ref = request.getHeader("referer").split("news-admin/")[1];
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.comment", result);
			redirect.addFlashAttribute("comment", comment);
		} else {
			commentService.save(comment);
		}
		return "redirect:" + ref;
	}

	@RequestMapping(value = "deletecomment")
	public String deleteComment(Model model, @RequestParam("id") long id, HttpServletRequest request)
			throws ServiceException {
		String ref = request.getHeader("referer").split("news-admin/")[1];
		commentService.delete(id);
		return "redirect:" + ref;
	}
}
