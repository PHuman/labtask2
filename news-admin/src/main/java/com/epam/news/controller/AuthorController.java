package com.epam.news.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.news.domain.Author;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

@Secured(value = "ROLE_ADMIN")
@Controller
public class AuthorController {
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "author")
	public String viewAuthors(Model model) throws ServiceException {
		List<Author> authors = authorService.loadAll();
		model.addAttribute("authors", authors);
		if (!model.containsAttribute("author")) {
			model.addAttribute("author", new Author());
		}
		return "author";
	}

	@RequestMapping(value = "expireauthor")
	public String expireAuthor(Model model, @RequestParam("id") long id) throws ServiceException {
		authorService.expire(id);
		return "redirect:author";
	}

	@RequestMapping(value = "updateauthor")
	public String updateAuthor(Model model, @ModelAttribute("author") @Valid Author author, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.author", result);
			redirect.addFlashAttribute("author", author);
		} else {
			authorService.update(author);
		}
		return "redirect:author";
	}

	@RequestMapping(value = "saveauthor")
	public String saveAuthor(Model model, @ModelAttribute("author") @Valid Author author, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.author", result);
			redirect.addFlashAttribute("author", author);
		} else {
			authorService.save(author);
		}
		return "redirect:author";
	}
}
