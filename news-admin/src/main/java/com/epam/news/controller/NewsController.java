package com.epam.news.controller;

import java.util.List;

import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.news.domain.Author;
import com.epam.news.domain.Comment;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.FacadeNewsService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;

@Secured(value = "ROLE_ADMIN")
@Controller
public class NewsController {
	private static final Logger LOG = Logger.getLogger(NewsController.class);
	private static int COUNT = 5;
	@Autowired
	private FacadeNewsService facadeNewsService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "index")
	public String index(Model model, @RequestParam(name = "page", defaultValue = "1", required = false) int page)
			throws ServiceException {
			long all = newsService.countNews();
			int count = (int) Math.ceil((double) all / COUNT);
			List<NewsTO> news = facadeNewsService.loadAllNews(page);
			model.addAttribute("tags", tagService.loadAll());
			model.addAttribute("authors", authorService.loadAll());
			model.addAttribute("news", news);
			model.addAttribute("count", count);
		return "index";
	}

	@RequestMapping(value = "edit")
	public String editNews(Model model, @RequestParam("id") long id) throws ServiceException {
		NewsTO news = facadeNewsService.loadNews(id);
		model.addAttribute("newsto", news);
		model.addAttribute("tags", tagService.loadAll());
		model.addAttribute("authors",
				authorService.loadAll().stream().filter(c -> c.getExpired() == null).collect(Collectors.toList()));
		return "add";
	}

	@RequestMapping(value = "update")
	public String updateNews(Model model, @ModelAttribute("newsto") NewsTO newsTO,
			@RequestParam(name = "tags", required = false) List<Long> tags) throws ServiceException {
		if (tags != null) {
			List<Tag> list = tags.stream().map(Tag::new).collect(Collectors.toList());
			newsTO.setTags(list);
		}
		newsService.editNews(newsTO);
		return "redirect:index?page=1";
	}

	@RequestMapping(value = "view")
	public String viewNews(Model model, @RequestParam("id") long id,
			@RequestParam(name = "tags", required = false) List<Long> tags,
			@RequestParam(name = "author", required = false) Long author) throws ServiceException {
		NewsTO news = facadeNewsService.loadNews(id);
		model.addAttribute("newsTO", news);
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", new Comment());
		}
		return "news";
	}

	@RequestMapping(value = "viewbysearch")
	public String viewNewsByCriteria(Model model, @RequestParam("id") long id,
			@RequestParam(name = "tags", required = false) List<Long> tags, @RequestParam("author") long author)
			throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthor(new Author(author));
		if (tags != null) {
			criteria.setTags(tags.stream().map(Tag::new).collect(Collectors.toList()));
		}
		NewsTO news = facadeNewsService.loadNewsByCriteria(criteria, id);
		model.addAttribute("newsTO", news);
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", new Comment());
		}
		return "news";
	}

	@RequestMapping(value = "delete")
	public String deleteNews(Model model, @RequestParam(name = "id", required = false) List<Long> id)
			throws ServiceException {
		if (id != null) {
			facadeNewsService.deleteNews(id);
		}
		return "redirect:index?page=1";
	}

	@RequestMapping(value = "savenews")
	public String saveNews(Model model, @ModelAttribute("newsto") @Valid NewsTO newsTO, BindingResult result,
			RedirectAttributes redirect, @RequestParam(name = "tags", required = false) List<Long> tags,
			@RequestParam("author") long author) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute("org.springframework.validation.BindingResult.newsto", result);
			redirect.addFlashAttribute("newsto", newsTO);
			return "redirect:new";
		}
		if (tags != null) {
			List<Tag> list = tags.stream().map(Tag::new).collect(Collectors.toList());
			newsTO.setTags(list);
		}
		newsTO.setAuthor(new Author(author));
		newsService.saveNews(newsTO);
		return "redirect:index?page=1";
	}

	@RequestMapping(value = "new")
	public String newNews(Model model) throws ServiceException {
		model.addAttribute("tags", tagService.loadAll());
		model.addAttribute("authors", authorService.loadAll());
		if (!model.containsAttribute("newsto")) {
			model.addAttribute("newsto", new NewsTO());
		}
		return "add";
	}

	@RequestMapping(value = "search")
	public String search(Model model, @RequestParam(name = "page", defaultValue = "1", required = false) int page,
			@RequestParam(name = "tags", required = false) List<Long> tags,
			@RequestParam(name = "author", required = false) Long author) throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		if (author == null && tags == null) {
			return "redirect:index?page=1";
		}
		if (author != null) {
			criteria.setAuthor(new Author(author));
		}
		if (tags != null) {
			criteria.setTags(tags.stream().map(Tag::new).collect(Collectors.toList()));
		}
		long all = newsService.countNewsBySearch(criteria);
		int count = (int) Math.ceil((double) all / COUNT);
		List<NewsTO> news = facadeNewsService.findAllNews(criteria, page);
		model.addAttribute("tags", tagService.loadAll());
		model.addAttribute("authors", authorService.loadAll());
		model.addAttribute("news", news);
		model.addAttribute("count", count);
		return "index";
	}
}
